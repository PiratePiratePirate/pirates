﻿using UnityEngine;
using System.Collections;

public static class MathHelpers
{
    /// <summary>
    /// takes a value and compares it against the step value returns either the lowStep value or HighStep Value
    /// </summary>
    /// <param name="lowStep"></param>
    /// <param name="highStep"></param>
    /// <param name="stepValue"></param>
    /// <param name="valueToStep"></param>
    /// <returns></returns>
    public static float Step(float lowStep, float highStep, float stepValue, float valueToStep)
    {
        if (valueToStep >= stepValue)
            return highStep;

        return lowStep;
    }
}
