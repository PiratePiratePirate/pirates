﻿using UnityEngine;
using System.Collections;

public class Tuple<T1, T2>
{
    public Tuple(T1 first, T2 second)
    {
        First = first;
        Second = second;
    }

    public T1 First { get; set; }
    public T2 Second { get; set; }
}

public class Tuple<T1, T2, T3>
{
    public Tuple(T1 first, T2 second, T3 third)
    {
        First = first;
        Second = second;
        Third = third;
    }

    public T1 First { get; set; }
    public T2 Second { get; set; }
    public T3 Third { get; set; }
}

public class Tuple<T1, T2, T3, T4>
{
    public Tuple(T1 first, T2 second, T3 third, T4 fourth)
    {
        First = first;
        Second = second;
        Third = third;
        Fourth = fourth;
    }

    public T1 First { get; set; }
    public T2 Second { get; set; }
    public T3 Third { get; set; }
    public T4 Fourth { get; set; }
}

public class Tuple<T1, T2, T3, T4, T5>
{
    public Tuple(T1 first, T2 second, T3 third, T4 fourth, T5 fifth)
    {
        First = first;
        Second = second;
        Third = third;
        Fourth = fourth;
        Fifth = fifth;
    }

    public T1 First { get; set; }
    public T2 Second { get; set; }
    public T3 Third { get; set; }
    public T4 Fourth { get; set; }
    public T5 Fifth { get; set; }
}

public class Tuple<T1, T2, T3, T4, T5, T6>
{
    public Tuple(T1 first, T2 second, T3 third, T4 fourth, T5 fifth, T6 sixth)
    {
        First = first;
        Second = second;
        Third = third;
        Fourth = fourth;
        Fifth = fifth;
        Sixth = sixth;
    }

    public T1 First { get; set; }
    public T2 Second { get; set; }
    public T3 Third { get; set; }
    public T4 Fourth { get; set; }
    public T5 Fifth { get; set; }
    public T6 Sixth { get; set; }
}

public class Tuple<T1, T2, T3, T4, T5, T6, T7>
{
    public Tuple(T1 first, T2 second, T3 third, T4 fourth, T5 fifth, T6 sixth, T7 seventh)
    {
        First = first;
        Second = second;
        Third = third;
        Fourth = fourth;
        Fifth = fifth;
        Sixth = sixth;
        Seventh = seventh;
    }

    public T1 First { get; set; }
    public T2 Second { get; set; }
    public T3 Third { get; set; }
    public T4 Fourth { get; set; }
    public T5 Fifth { get; set; }
    public T6 Sixth { get; set; }
    public T7 Seventh { get; set; }
}

public class Tuple<T1, T2, T3, T4, T5, T6, T7, T8>
{
    public Tuple(T1 first, T2 second, T3 third, T4 fourth, T5 fifth, T6 sixth, T7 seventh, T8 eighth)
    {
        First = first;
        Second = second;
        Third = third;
        Fourth = fourth;
        Fifth = fifth;
        Sixth = sixth;
        Seventh = seventh;
        Eighth = eighth;
    }

    public T1 First { get; set; }
    public T2 Second { get; set; }
    public T3 Third { get; set; }
    public T4 Fourth { get; set; }
    public T5 Fifth { get; set; }
    public T6 Sixth { get; set; }
    public T7 Seventh { get; set; }
    public T8 Eighth { get; set; }
}

public class Tuple<T1, T2, T3, T4, T5, T6, T7, T8, T9>
{
    public Tuple(T1 first, T2 second, T3 third, T4 fourth, T5 fifth, T6 sixth, T7 seventh, T8 eighth, T9 ninth)
    {
        First = first;
        Second = second;
        Third = third;
        Fourth = fourth;
        Fifth = fifth;
        Sixth = sixth;
        Seventh = seventh;
        Eighth = eighth;
        Ninth = ninth;
    }

    public T1 First { get; set; }
    public T2 Second { get; set; }
    public T3 Third { get; set; }
    public T4 Fourth { get; set; }
    public T5 Fifth { get; set; }
    public T6 Sixth { get; set; }
    public T7 Seventh { get; set; }
    public T8 Eighth { get; set; }
    public T9 Ninth { get; set; }
}
