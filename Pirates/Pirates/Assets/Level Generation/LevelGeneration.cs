﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


using LibNoise.Generator;

public class LevelGeneration : MonoBehaviour
{
    /// <summary>
    /// used to generate a portion of the level mesh
    /// </summary>
    public class MeshGenerator
    {
        bool m_IsReady;
        public bool IsReady
        {
            get { return m_IsReady; }
        }

        static Vector3 m_WorldScale = new Vector3(10.0f, 60.0f, 10.0f);
        public static Vector3 WorldScale
        {
            get { return m_WorldScale; }
            set { m_WorldScale = value; }
        }

        static Perlin m_PerlinNoise;
        public static Perlin PerlinNoise
        {
            get { return m_PerlinNoise; }
            set { m_PerlinNoise = value; }
        }

        Vector3 m_PerlinNoisePosition = Vector2.zero;
        public Vector3 PerlinNoisePosition
        {
            get { return m_PerlinNoisePosition; }
            set { m_PerlinNoisePosition = value; }
        }

        static Vector3 m_PerlinNoiseIncrement = new Vector3(0.01f, 0.01f, 0.01f);
        public static Vector3 PerlinNoiseIncrement
        {
            get { return m_PerlinNoiseIncrement; }
            set { m_PerlinNoiseIncrement = value; }
        }


        byte m_MeshWidth;
        byte m_MeshHeight;

        byte m_PosX = 0;
        byte m_PosY = 0;

        Vector3[] m_MeshVerticies;
        public Vector3[] MeshVerticies
        {
            get 
            {
                if (!IsReady)
                    return null;

                return m_MeshVerticies; 
            }
        }
        int[] m_MeshIndicies;
        public int[] MeshIndicies
        {
            get 
            {
                if (!IsReady)
                    return null;
                return m_MeshIndicies; 
            }
        }

        int m_IndecieIndex = 0;

        public MeshGenerator()
        {

        }

        public void StartGeneration(Vector2 perlinStartCoords, byte meshWidth, byte meshHeight)
        {
            if (meshWidth * meshHeight > 65000)
                Debug.LogError("You tried to make a mesh with too many verts dumbass");

            m_PerlinNoisePosition = perlinStartCoords;
            m_MeshWidth = meshWidth;
            m_MeshHeight = meshHeight;

            m_MeshVerticies = new Vector3[m_MeshWidth * meshHeight];
            m_MeshIndicies = new int[(m_MeshWidth - 1) * (m_MeshHeight - 1) * 2 * 3];
        }

        /// <summary>
        /// continue creating the mesh
        /// returns true if finished the generation
        /// </summary>
        /// <returns></returns>
        public bool updateGeneration()
        {
            int c = 0;
            for (; m_PosX < m_MeshWidth;)
            {
                for (; m_PosY < m_MeshHeight;)
                {
                    //vertex position
                    m_MeshVerticies[m_PosX * m_MeshHeight + m_PosY] = new Vector3(  m_PosX * m_WorldScale.x,
                                                                                    (float)m_PerlinNoise.GetValue((m_PerlinNoisePosition + new Vector3(m_PosX * m_PerlinNoiseIncrement.x, 1.0f * m_PerlinNoiseIncrement.y, m_PosY * m_PerlinNoiseIncrement.z))) * m_WorldScale.y, 
                                                                                   m_PosY * m_WorldScale.z);

					//m_MeshVerticies[m_PosX * m_MeshHeight + m_PosY] = new Vector3(	m_PosX * m_WorldScale.x,
                    //                                                              	1.0f * m_WorldScale.y, 
                    //                                                             	m_PosY * m_WorldScale.z);

                    //trianlge creation
                    if(m_IndecieIndex < m_MeshIndicies.Length)
                    {
                        if (m_PosX < m_MeshWidth - 1 && m_PosY < m_MeshHeight - 1)
                        {
                            m_MeshIndicies[m_IndecieIndex++] = (m_PosX * (m_MeshHeight)) + (m_PosY);              //  |--/
							m_MeshIndicies[m_IndecieIndex++] = (m_PosX * (m_MeshHeight)) + (m_PosY + 1);          //  | /
							m_MeshIndicies[m_IndecieIndex++] = ((m_PosX + 1) * (m_MeshHeight)) + (m_PosY + 1);    //  |/

							m_MeshIndicies[m_IndecieIndex++] = (m_PosX * (m_MeshHeight)) + (m_PosY);              //    /|
							m_MeshIndicies[m_IndecieIndex++] = ((m_PosX + 1) * (m_MeshHeight)) + (m_PosY + 1);      //   / |
							m_MeshIndicies[m_IndecieIndex++] = ((m_PosX + 1) * (m_MeshHeight)) + (m_PosY);          //  /--|
                        }
                    }

                    m_PosY++;
                    c++;
                }
				m_PosY = 0;
                m_PosX++;
                if (c >= 1000)
                    return m_IsReady = false;
            }
            return m_IsReady = true;
        }
    }

    //public Terrain i_Terrain;
    //public ComputeShader i_MeshGenerationShader;
    //public Texture2D i_Heightmap;

    //Vector3[] m_VerticiesBuffer;//buffer for the compute shader to fill with vertex position data
    //int[] m_IndeciesBuffer;//buffer for the comput shader to fill with tirangle winding data

    MeshGenerator m_MeshGenerator = new MeshGenerator();

    // Use this for initialization
    void Start ()
    {
        //NoiseMap - F1.45 - L1.59 - O6 - P0.5 - S1460 - S0.6
        MeshGenerator.PerlinNoise = new Perlin(1.45, 1.59, 0.5, 6, Random.Range(0, 1000000), LibNoise.QualityMode.Medium);
        m_MeshGenerator.StartGeneration(Vector2.zero, 254, 254);

        //i_Terrain.drawHeightmap = i_Heightmap;
        /*
        
        */
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (m_MeshGenerator.IsReady)
            return;

        if (!m_MeshGenerator.updateGeneration())
            return;

        Mesh mesh = new Mesh();
        MeshRenderer meshRenderer = gameObject.AddComponent<MeshRenderer>();
        MeshFilter meshFilter = gameObject.AddComponent<MeshFilter>();
        mesh.vertices = m_MeshGenerator.MeshVerticies;
        mesh.triangles = m_MeshGenerator.MeshIndicies;

        mesh.RecalculateNormals();
        mesh.RecalculateBounds();

        meshFilter.mesh = mesh;
	}

    /*void CreateVertexAndIndexBufferData()
    {
        int kernelHandle = i_MeshGenerationShader.FindKernel("CSMain");

        i_MeshGenerationShader.SetTexture(kernelHandle, "Heightmap", i_Heightmap);

		m_VerticiesBuffer = new Vector3[1024 * 1024];
		m_IndeciesBuffer = new int[1024 * 1024 * 3];

        ComputeBuffer verts = new ComputeBuffer(1024 * 1024, sizeof(float) * 3);
        ComputeBuffer indecies = new ComputeBuffer(1024 * 1024 * 3, sizeof(int));

        i_MeshGenerationShader.SetBuffer(kernelHandle, "VerticiesBuffer", verts);        
        i_MeshGenerationShader.SetBuffer(kernelHandle, "IndeciesBuffer", indecies);

        i_MeshGenerationShader.Dispatch(kernelHandle, 1024 / 16, 1024 / 16, 1);

        verts.GetData(m_VerticiesBuffer);
        indecies.GetData(m_IndeciesBuffer);

        verts.Release();
        indecies.Release();
    }*/
}

