﻿using UnityEngine;
using System.Collections;

using LibNoise;
using LibNoise.Generator;

public class AnimatedTexture : MonoBehaviour 
{
    Texture2D m_Texture;
    RidgedMultifractal m_Noise = new RidgedMultifractal();

    float m_Time = 0.0f;

    public double i_frequency = 1.0;
    public double i_lacunarity = 2.0;
    public QualityMode i_quality = QualityMode.Medium;
    public int i_octaveCount = 6;
    public int i_seed = 1458;

    public Vector2 i_NoiseStep = new Vector2(0.01f, 0.01f);
    public float i_TimeScale = 0.01f;

	// Use this for initialization
	void Start () 
    {
        m_Texture = new Texture2D(50, 50, TextureFormat.RGBA32, false);
        m_Texture.alphaIsTransparency = true;

        gameObject.GetComponent<Renderer>().materials[0].SetTexture(0, m_Texture);
	}
	
	// Update is called once per frame
	void Update () 
    {
        m_Noise.Frequency = i_frequency;
        m_Noise.Lacunarity = i_lacunarity;
        m_Noise.Quality = i_quality;
        m_Noise.OctaveCount = i_octaveCount;
        m_Noise.Seed = i_seed;

        m_Time += Time.deltaTime;
        Vector3 noisePosR;
        Vector3 noisePosG;
        Vector3 noisePosB;
	    for(int x = 0; x < m_Texture.width; x++)
        {
            for(int y = 0; y < m_Texture.height; y++)
            {
                noisePosR = new Vector3(x * i_NoiseStep.x, y * i_NoiseStep.y, m_Time * i_TimeScale);
                noisePosG = new Vector3(x * i_NoiseStep.x, m_Time * i_TimeScale, y * i_NoiseStep.y);
                noisePosB = new Vector3(m_Time * i_TimeScale, x * i_NoiseStep.x, y * i_NoiseStep.y);

                m_Texture.SetPixel(x, y, new Color((float)m_Noise.GetValue(noisePosR), (float)m_Noise.GetValue(noisePosG), (float)m_Noise.GetValue(noisePosB), 1.0f));
            }
        }
        m_Texture.Apply();
	}
}
