﻿using UnityEngine;
using System.Collections;

using LibNoise;
using LibNoise.Generator;

using System.IO;

public class NoiseTest: MonoBehaviour
{
    public Material i_TestMaterial;

    public int i_TextureWidth = 1024;
    public int i_TextureHeight = 1024;
    public TextureFormat i_TextureFormat = TextureFormat.RGBA32;
    public bool i_Mipmap = false;
    Texture2D m_Texture;

	ModuleBase m_NoiseModule;
    public enum ModuleType
    {
        Billow,
        Perlin,
        RidgedMultifractal
    };

    public ModuleType i_NoiseType;

    public double i_frequency = 1.0;
    public double i_lacunarity = 2.0;
    public QualityMode i_quality = QualityMode.Medium;
    public int i_octaveCount = 6;
    public double i_persistence = 0.5;
    public int i_seed = 1458;

    public Vector2 i_XY_Scale = new Vector2(0.01f, 0.01f);

    public bool i_IsStepped = false;
    public float i_StepValue = 0.75f;

    public string i_SavePath = "/Level Generation";
    public string i_SaveName = "/NoiseMap";

    // Use this for initialization
    void Start () 
    {
		m_Texture = new Texture2D (i_TextureWidth, i_TextureHeight, i_TextureFormat, i_Mipmap);

        switch (i_NoiseType)
        {
            case ModuleType.Billow:
                m_NoiseModule = new Billow();
                break;
            case ModuleType.Perlin:
                m_NoiseModule = new Perlin();
                break;
            case ModuleType.RidgedMultifractal:
                m_NoiseModule = new RidgedMultifractal();
                break;
        }
	}

    int x = 0;

	// Update is called once per frame
	void Update () 
    {
        switch (i_NoiseType)
        {
            case ModuleType.Billow:
                ((Billow)m_NoiseModule).Frequency = i_frequency;
                ((Billow)m_NoiseModule).Lacunarity = i_lacunarity;
                ((Billow)m_NoiseModule).Quality = i_quality;
                ((Billow)m_NoiseModule).OctaveCount = i_octaveCount;
                ((Billow)m_NoiseModule).Persistence = i_persistence;
                ((Billow)m_NoiseModule).Seed = i_seed;
                break;
            case ModuleType.Perlin:
                ((Perlin)m_NoiseModule).Frequency = i_frequency;
                ((Perlin)m_NoiseModule).Lacunarity = i_lacunarity;
                ((Perlin)m_NoiseModule).Quality = i_quality;
                ((Perlin)m_NoiseModule).OctaveCount = i_octaveCount;
                ((Perlin)m_NoiseModule).Persistence = i_persistence;
                ((Perlin)m_NoiseModule).Seed = i_seed;
                break;
            case ModuleType.RidgedMultifractal:
                ((RidgedMultifractal)m_NoiseModule).Frequency = i_frequency;
                ((RidgedMultifractal)m_NoiseModule).Lacunarity = i_lacunarity;
                ((RidgedMultifractal)m_NoiseModule).Quality = i_quality;
                ((RidgedMultifractal)m_NoiseModule).OctaveCount = i_octaveCount;
                ((RidgedMultifractal)m_NoiseModule).Seed = i_seed;
                break;
        }


        int c = 0;
        do
        {
            for (int z = 0; z < 1024; z++)
            {
                float value = (1.0f + Mathf.Clamp((float)m_NoiseModule.GetValue(new Vector3(x * i_XY_Scale.x, 1, z * i_XY_Scale.y)), -1.0f, 1.0f)) / 2.0f;

                if(i_IsStepped)
                    value = MathHelpers.Step(0.0f, 1.0f, i_StepValue, value);

                m_Texture.SetPixel(x, z, new Color(value, value, value, 1.0f));
            }

            x++;
            if (x >= 1024)
                x = 0;

            c++;
        } while (c < 3);
        m_Texture.Apply();

        i_TestMaterial.SetTexture(0, m_Texture);

        if (!Input.GetKeyDown(KeyCode.S))
            return;
        SaveTexture();
	}

    void SaveTexture()
    {
        byte[] textureBytes = m_Texture.EncodeToPNG();
        File.WriteAllBytes(Application.dataPath + i_SavePath + i_SaveName + ".png", textureBytes);
    }
}
