﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class AssetSaver
{
    public string i_FilePath = "Resources\\Generated";
    public string i_AssetName = "";
    string m_FileType = ".asset";

    public Object Asset = null;
	// Use this for initialization
	void Start ()
    {
        if (i_AssetName.Equals(""))
            i_AssetName = "\\No Name " + Time.fixedTime;


        saveAsset();
    }	

	protected virtual void saveAsset()
    {
        AssetDatabase.CreateAsset(Asset, "Assets/" + i_AssetName + ".asset");
    }
}
