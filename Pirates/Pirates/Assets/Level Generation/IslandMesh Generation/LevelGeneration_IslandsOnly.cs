﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using LibNoise;
using LibNoise.Generator;

public class LevelGeneration_IslandsOnly : MonoBehaviour 
{
    public int i_WorldSizeX = 2048;
    public int i_WorldSizeY = 2048;

    public Vector3 i_WorldScale = new Vector3(10, 25, 10);

    [Range(-1.0f, 1.0f)]
    public float i_IslandStep = 0.65f;

    [Range(-1.0f, 1.0f)]
    public float i_WaterLevel = 0.7f;

    public Vector3 i_PerlinStep = new Vector3(0.01f, 0.01f, 0.01f);

    public PerlinConstructor i_PerlinConstructor = new PerlinConstructor();
    Perlin m_NoiseGenerator;

    public Material i_IslandMaterial;

    List<Island> m_Islands = new List<Island>();

    int m_XIndex = 0;
    int m_YIndex = 0;

	// Use this for initialization
	void Start () 
    {
        m_NoiseGenerator = new Perlin(ref i_PerlinConstructor);
    }

    //returns true if the current islands are done building
    bool buildIslands()
    {
        for (int i = 0; i < m_Islands.Count; i++)
        {
            if (!m_Islands[i].IsFinished)
            {
                if(m_Islands[i].buildIsland())
                {
                    GameObject island = new GameObject("Island: " + i);

                    island.transform.parent = this.transform;

                    Mesh mesh = new Mesh();
                    MeshRenderer meshRenderer = island.AddComponent<MeshRenderer>();
                    MeshFilter meshFilter = island.AddComponent<MeshFilter>();
                    mesh.vertices = m_Islands[i].getVerticies();
                    mesh.triangles = m_Islands[i].getIndicies();

                    mesh.RecalculateNormals();
                    mesh.RecalculateBounds();

                    meshFilter.mesh = mesh;
                    meshRenderer.material = new Material(i_IslandMaterial);

                    island.AddComponent<MeshCollider>().sharedMesh = mesh;
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        return true;//all islands are done builing so return true
    }

	// Update is called once per frame
	void Update () 
    {
        if (!buildIslands())//wait for the current island to be done builind before checking for more islands
        {
            return;
        }

        for (; m_XIndex < i_WorldSizeX; m_XIndex++)
        {
            m_YIndex = 0;
            for (; m_YIndex < i_WorldSizeY; m_YIndex++)
            {
                if (checkNoiseCoords(m_XIndex, m_YIndex))
                    return;
                
            }
        }
        
        //TODO: populate the world

        GameObject.Destroy(this);
        
	}

    //returns true if the noise value is valid for island creation
    protected bool checkNoiseCoords(int x, int y, int z = 0)
    {
        float value = (float)m_NoiseGenerator.GetValue(new Vector3(m_XIndex * i_PerlinStep.x, m_YIndex * i_PerlinStep.y, 0.0f));

        //will the point be close enough to the surface?
        if (value < i_IslandStep)
            return false;

        //point is either part of an existing island or a new island
        for (int i = 0; i < m_Islands.Count; i++)
        {//check if the point is part of an exisiting island
            if (m_Islands[i].containsPoint(m_XIndex, m_YIndex))
                return false;//point is part of an exisiting island
        }      

        //point is a new island
        m_Islands.Add(new Island(this));// create the new island and add the starting pointS
        Island.Node_Constructor newNode = new Island.Node_Constructor();

        newNode.Position = new Vector3(m_XIndex * i_WorldScale.x, value * i_WorldScale.y, m_YIndex * i_WorldScale.z);
        newNode.PosX = m_XIndex;
        newNode.PosY = m_YIndex;

        m_Islands[m_Islands.Count - 1].addPoint(newNode);
        return true;
    }

    protected class Island
    {
        LevelGeneration_IslandsOnly m_Container = null;

        List<Node> m_NewNodes = new List<Node>();
        Dictionary<Tuple<int, int>, Node> m_Nodes = new Dictionary<Tuple<int, int>, Node>();

        Vector3[] m_Verticies = null;
        int[] m_Indecies = null;

        bool m_IsFinished = false;
        public bool IsFinished
        {
            get { return m_IsFinished; }
        }

        public Island(LevelGeneration_IslandsOnly container)
        {
            m_Container = container;
        }

        /// <summary>
        /// returns true if all posiible points have been added to the island
        /// </summary>
        /// <returns></returns>
        public bool buildIsland()
        {
            int i = 0;//counter to limit the number of nodes created in one update
            while (m_NewNodes.Count > 0)
            {
                Node currentNode = m_NewNodes[m_NewNodes.Count - 1];
                m_NewNodes.RemoveAt(m_NewNodes.Count - 1);

                for (int x = -1; x <= 1; x++)
                {
                    for (int y = -1; y <= 1; y++)
                    {
                        if (x == 0 && y == 0)
                            continue;//dont need to check the one were working with

                        //check if the curent neightbor has already been added
                        if (containsPoint(currentNode.PosX + x, currentNode.PosY + y))
                            continue;

                        Node_Constructor nodeConstructor = new Node_Constructor();

                        nodeConstructor.PosX = currentNode.PosX + x;
                        nodeConstructor.PosY = currentNode.PosY + y;

                        nodeConstructor.Position = new Vector3(nodeConstructor.PosX * m_Container.i_WorldScale.x,
                                                                (float)m_Container.m_NoiseGenerator.GetValue
                                                                (
                                                                    new Vector3
                                                                    (
                                                                        nodeConstructor.PosX * m_Container.i_PerlinStep.x,
                                                                        nodeConstructor.PosY * m_Container.i_PerlinStep.y,
                                                                        0.0f
                                                                    )
                                                                ) * m_Container.i_WorldScale.y,
                                                                nodeConstructor.PosY * m_Container.i_WorldScale.z);

                        Node newNode = new Node(this, nodeConstructor);

                        m_Nodes.Add(new Tuple<int, int>(nodeConstructor.PosX, nodeConstructor.PosY), newNode);
                        if (newNode.Position.y / m_Container.i_WorldScale.y >= m_Container.i_IslandStep)
                        {
                            m_NewNodes.Add(newNode);
                        }
                    }
                }
                i++;

                if (i >= 100)
                    return false;
            }

            if (m_NewNodes.Count > 0)
            {
                return false;
            }
            else
            {
                createVertexIndecies();
                m_IsFinished = true;
                Debug.Log("Done Island: " + m_Container.m_Islands.Count);
                return true;
            }
        }

        public void addPoint(Node_Constructor newNode)
        {
            if (containsPoint(newNode.PosX, newNode.PosY))
                return;

            m_NewNodes.Add(new Node(this, ref newNode));
            m_Nodes.Add(new Tuple<int, int>(newNode.PosX, newNode.PosY), m_NewNodes[m_NewNodes.Count - 1]);
        }

        public bool containsPoint(Vector3 point)
        {
            Dictionary<Tuple<int, int>, Node>.ValueCollection.Enumerator nodes = m_Nodes.Values.GetEnumerator();

            for (int i = 0; i < m_Nodes.Count; i++)
            {
                nodes.MoveNext();
                if (nodes.Current.Position == point)
                    return true;
            }
            return false;
        }

        public bool containsPoint(int x, int y, int z = 0)
        {
            Dictionary<Tuple<int, int>, Node>.ValueCollection.Enumerator nodes = m_Nodes.Values.GetEnumerator();

            for (int i = 0; i < m_Nodes.Count; i++)
            {
                nodes.MoveNext();
                if (nodes.Current.PosX == x && nodes.Current.PosY == y)
                    return true;
            }
            return false;
        }

        public Vector3[] getVerticies()
        {
            return m_Verticies;
        }

        public int[] getIndicies()
        {
            return m_Indecies;
        }

        void createVertexIndecies()
        {
            Dictionary<Tuple<int, int>, Node>.KeyCollection.Enumerator keys = m_Nodes.Keys.GetEnumerator();

            m_Verticies = new Vector3[m_Nodes.Count];

            //indicies is a list since i cant think of a way to predice the number of tris since they dont form a rectangle any more
            List<int> indicies = new List<int>();

            //set all the verticies
            for (int i = 0; i < m_Nodes.Count; i++)
            {
                keys.MoveNext();
                m_Verticies[i] = m_Nodes[keys.Current].Position;
                m_Nodes[keys.Current].Index = i;
            }

            //now that all vertex indicies have been assigned we can create our index buffer

            keys = m_Nodes.Keys.GetEnumerator();//reset the enumerator

            Tuple<int, int> topLeft = new Tuple<int, int>(0, 0);
            Tuple<int, int> topMiddle = new Tuple<int, int>(0, 0);
            Tuple<int, int> middleRight = new Tuple<int, int>(0, 0);

            for (int i = 0; i < m_Nodes.Count; i++)
            {
                keys.MoveNext();
                topLeft.First = keys.Current.First - 1;   //x
                topLeft.Second = keys.Current.Second + 1; //y

                topMiddle.First = keys.Current.First;       //x
                topMiddle.Second = keys.Current.Second + 1; //y

                middleRight.First = keys.Current.First + 1; // x
                middleRight.Second = keys.Current.Second;   // y

                //tri = current node -> top left neighbor -> top middle neighbor
                if (containsPoint(topLeft.First, topLeft.Second) && containsPoint(topMiddle.First, topMiddle.Second))
                {//the other required points for the tri exisit
                    indicies.Add(i);
                    indicies.Add(getNode(topLeft).Index);
                    indicies.Add(getNode(topMiddle).Index);
                }

                //tri = current node -> top middle neighbor -> middle right neighbor
                if (containsPoint(middleRight.First, middleRight.Second) && containsPoint(topMiddle.First, topMiddle.Second))
                {//the other required points for the tri exisit
                    indicies.Add(i);
                    indicies.Add(getNode(topMiddle).Index);
                    indicies.Add(getNode(middleRight).Index);
                }
            }
            //move the indices to the array
            m_Indecies = indicies.ToArray();
        }

        Node getNode(Tuple<int,int> coords)
        {
            return getNode(coords.First, coords.Second);
        }

        Node getNode(int x, int y)
        {
            if (!containsPoint(x, y))
                return null;

            Dictionary<Tuple<int, int>, Node>.ValueCollection.Enumerator nodes = m_Nodes.Values.GetEnumerator();

            for (int i = 0; i < m_Nodes.Count; i++)
            {
                nodes.MoveNext();
                if (nodes.Current.PosX == x && nodes.Current.PosY == y)
                    return nodes.Current;
            }

            return null;
        }

        public class Node_Constructor
        {
            public int Index{ get; set; }
            public Vector3 Position { get; set; }
            public int PosX { get; set; }
            public int PosY { get; set; }
        }

        protected class Node
        {
            Island m_Container = null;

            //array of neighbors
            Node[] m_Neighbors = new Node[(int)Neighbor.Count];

            int m_Index;
            public int Index
            {
                get { return m_Index; }
                set { m_Index = value; }
            }

            Vector3 m_Position;
            public Vector3 Position
            {
                get { return m_Position; }
                set { m_Position = value; }
            }

            int m_PosX = 0;
            public int PosX
            {
                get { return m_PosX; }
                set { m_PosX = value; }
            }

            int m_PosY = 0;
            public int PosY
            {
                get { return m_PosY; }
                set { m_PosY = value; }
            }

            public Node(Island container, ref Node_Constructor constructor)
            {
                m_Container = container;

                m_Index = constructor.Index;
                m_Position = constructor.Position;
                m_PosX = constructor.PosX;
                m_PosY = constructor.PosY;
            }

            public Node(Island container, Node_Constructor constructor)
            {
                m_Container = container;

                m_Index = constructor.Index;
                m_Position = constructor.Position;
                m_PosX = constructor.PosX;
                m_PosY = constructor.PosY;
            }

            public void setNeighbor(Neighbor neighbor, Node node)
            {
                m_Neighbors[(int)neighbor] = node;
            }

            public Node getNeighbor(Neighbor neighbor)
            {
                return m_Neighbors[(int)neighbor];
            }
        }
    }
}

public enum Neighbor
{
    Top_Left,
    Top_Middle,
    Top_Right,
    Middle_Left,
    Middle_Right,
    Bottom_Left,
    Bottom_Middle,
    Bottom_Right,
    Count
};
